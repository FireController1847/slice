/**
 * Copyright (c) 2018, Visual Fire Development  All Rights Reserved
 * Copyrights licensed under the GNU General Public License v3.0.
 * See the accompanying LICENSE file for terms.
 */

const { Command } = require("discord-akairo");
const chrono = require("chrono-node");
const moment = require("moment-timezone");

class Date extends Command {
  constructor() {
    super("date", {
      aliases: ["date"],
      description: "Converts many different formats to date to a readable version.",
      typing: true,
      args: [
        {
          id: "input",
          match: "rest"
        }
      ]
    });
  }
  exec(m, { input }) {
    const date = chrono.parseDate(input);
    if (!date) return m.channel.send("I'm sorry, I'm not sure what that date is.");
    return m.channel.send(`That date is ${moment(date).tz("America/Denver").format("dddd, MMMM Do, YYYY [at] h:mm A zz")}.`);
  }
}

module.exports = Date;